# WEDGE: Wireless Ergonomic Dynamic Gesture Enhancer

**Team Members:** Dominic Barrett, Colin Keenan, Ye Tian, Lifei Wang, Erika Zhou

**A multi-purpose VR/AR hardware peripheral by [MIT Reality Hack 2024](https://mit-reality-hack-2024.devpost.com/) Team 13 - now in wedge form!**


The "WEDGE" is a wireless, wedge-shaped peripheral device designed for bidirectional interaction with extended reality (XR) environments. It integrates with an Android application running on standalone headsets such as the Oculus/Meta Quest 2. The device is equipped with remappable buttons and LED outputs, managed by an ESP-32 microcontroller.

The Unity VR application in this repository is meant to serve as a proof-of-concept of several use cases for the WEDGE.

## Setup

**Using Singularity**

This project uses the ESP-32 microcontroller provided by the MIT Reality Hack/Hardware Hack Track. It talks with Unity using the open source library [The Singularity](https://github.com/VRatMIT/TheSingularity-Unity), and has been built and tested on an Oculus Quest 2.

The WEDGE should be paired with your VR/AR headset via Bluetooth.

### Hardware Required

- Oculus/Meta Quest 2 headset, or another compatible headset such as Meta Quest 3 or Meta Quest Pro
- The WEDGE
  - Featuring ESP-32 microcontroller, buttons, and LED lights

### Software Dependencies

- Unity game engine, editor version 2022.3.12f1 LTS
- Unity XR Interaction Toolkit (version 2.5.2)
- The Singularity https://github.com/VRatMIT/TheSingularity-Unity 
- Newtonsoft JSON https://www.newtonsoft.com/json 
- SideQuest, for loading the Unity app onto the headset.

## Run
**Try the demo Unity application**
1. Build the app in Unity.
1. With your headset plugged into your computer, load the app onto your headset using SideQuest.
1. Disconnect your headset to use standalone mode.
1. Pair the WEDGE with your headset using Bluetooth.
1. Place or hold the WEDGE in a comfortable position for your use.
1. Go to Library>Unknown Sources in your headset.
1. Launch the app Wedge13Demo. (It may have a version number after it, too.)
1. Use the buttons on the WEDGE to interact with the demos.
1. Use the trigger button on the VR hand controller to move to the next demo. Use the grip button to move to the previous demo. When changing the demo, one of the LEDs lights up as an indicator to those outside the VR experience.
> Changing the demos may be controlled by someone who's not wearing the VR headset, which could help with facilitation of VR exhibits.

## Shout-Outs

Thank you to mentors Peter Sassaman for his help with hardware, Isa Cantarero for her help with discussing project ideas, and the other mentors who engaged with us at our table. Thank you to Lucas De Bonet for help with The Singularity, hardware, and coding.
Special thanks to Lucas De Bonet and T Chen for your hard work running the Hardware Hack.

# Tools Used
- 3D printed plastic side pieces 
- Arcade buttons, LED lights, wires, breadboard, power pack
- Foam board
- Cardboard, paper, and duct tape used during rapid prototyping. 
- Arduino IDE https://www.arduino.cc/en/software  
- Bezi https://www.bezi.com/  
- Bezi Unity Bridge https://github.com/bezi3d/bezi_unity_bridge.git?path=/BeziUnityBridgePlugin#main  
- Bezi Unity Integration  
- ChatGPT/GPT-4 https://chat.openai.com/ Used for help with coding, Unity development, and/or miscellaneous tasks. 
- ESP-32 microcontroller  
- Figma https://www.figma.com/  
- glTFast Unity package. Needed for Bezi-Unity integration. 
- Meshy, for generating 3D models https://www.meshy.ai/  
- Newtonsoft JSON framework https://www.newtonsoft.com/json  
- Oculus/Meta Quest 2 VR headset   
- Prusa Slicer & MK3 printer for 3D printing  
- Rhino https://www.rhino3d.com/   
- RunwayML audio cleanup https://runwayml.com/  
- SideQuest https://sidequestvr.com/   
- Suno AI text-to-Audio diffusion model https://www.suno.ai/  
- The Singularity https://github.com/VRatMIT/TheSingularity-Unity  
- Unity game engine  
- Unity XR Interaction Toolkit https://docs.unity3d.com/Packages/com.unity.xr.interaction.toolkit@2.5/manual/index.html  

# Assets 
- Wagner Modern Font for Logo  
- “Raindrops in Super Slow Motion” (https://www.videvo.net/video/raindrops-in-super-slow-motion/3313/#rs=video-box) by Beachfront is licensed under [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/deed.en).  
- "Bubble Pop Gallery" (https://skfb.ly/o6FUz) by ellasuper is licensed under Creative Commons Attribution 4.0 [http://creativecommons.org/licenses/by/4.0/](http://creativecommons.org/licenses/by/4.0/).  
This 3D model was used for the demo environment. 
- “Lonely Tree at Sunset (slow motion) CC-BY NatureClip” (https://www.videvo.net/video/lonely-tree-at-sunset-slow-motion-cc-by-natureclip/2148/#rs=video-box) by NatureClip is licensed under [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/deed.en).  
- “Macro Seedling Time-Lapse CC-BY NatureClip” (https://www.videvo.net/video/macro-seedling-time-lapse-cc-by-natureclip/2150/#rs=video-box) by NatureClip is licensed under [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/deed.en).  
- “Morning Clouds CC-BY NatureClip” (https://www.videvo.net/video/morning-clouds-cc-by-natureclip/2144/#rs=video-box) by NatureClip is licensed under [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/deed.en). 

# References/Resources
1. “Bezi + Unity Integration Demo” by Bezi on YouTube. https://www.youtube.com/watch?v=H-Xxes3UGSk&t=24s  
1. “Change author information of previous commits.” gist by IamFaizanKhalid on GitHub. https://gist.github.com/IamFaizanKhalid/c0f0475a58a2e331735feb1dd1c7ac59  
    - Note: Erika Zhou made pushes to our Codeberg repo and realized that her Git username and email were set to something different than what she used for her Codeberg account. This was because she had her Git username set to her university login username (zhoue) and had a different email configured in Git. She didn’t change the configuration before starting to push to Codeberg. She was using a computer borrowed from a university student organization. After some time after finding out about this, the username and email were configured, and Erika spent some time to change the author information of previous commits that had the wrong Git username. Please ask Erika Zhou if you would like clarification or further explanation. I apologize for my mistake. 
1. “ESPs, Arduino, the Singularity” slides from MIT Reality Hack 2024 workshop https://docs.google.com/presentation/d/1oQrXYDs7UZmUIMiwRm-VoYjyhoI2zvGtCqbZ5bfcDvg/edit#slide=id.p  
1. "Unity Integration" - Bezi Documentation https://bezi3d.notion.site/Unity-Integration-f897f3ad575c4cfb9e685b51122893c5#7dca916b770c4c23b6d58d34d06cd1d5  
1. “XRTK for Oculus Quest 2” slides from MIT Reality Hack 2024 workshop https://docs.google.com/presentation/d/1CiPYIz61ANj-T_KTU9drycy6XUjfOIP1mAXNGilUi1I/edit#slide=id.g1cf1714baaf_0_92  
