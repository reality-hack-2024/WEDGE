//This is example code using bi-directional communciation with Unity via Bluetooth
//Assuming the Singularity is installed and working in your Unity project and Quest headset
//Functional breakthrough for MIT Reality Hack/Hardware Hack team 13

#include <BluetoothSerial.h>
BluetoothSerial BTSerial;

//The variable we will send
int button1State;

void setup() {
  // put your setup code here, to run once:
  //Serial.begin(9600); //having the normal serial can be useful for debugging when needed
  BTSerial.begin("WORKINGWEDGE-ESP32"); //Quotes are "Name of your bluetooth device"
  pinMode(2,OUTPUT); //Pin 2 on our provided ES32's uses the onboard blue LED
  pinMode(15,OUTPUT);
  
  pinMode(19,INPUT); //button press on pin 19
  pinMode(4,INPUT); //button press on pin 4
}

void loop() {
  
  //Sending values to Unity
  //Button press from uC to destination

  int digitalValue1 = digitalRead(4);
  
  int digitalValue2 = digitalRead(19);
  // Serial.println(digitalValue);
  delay(1);

  String stringToUnity = String(digitalValue1)+","+String(digitalValue2);
  delay(50);

  //BTSerial.println(String(digitalValue1)+","+String(digitalValue2)); //this sends constant stream via BT
  BTSerial.println(stringToUnity); //this sends constant stream via BT




  if (digitalValue1 == HIGH) {
    button1State=1;
    //you could send 'digitalValue' straight, or do necessary smoothing/processing/etc here

    //Serial.println(button1State);
    //BTSerial.println(button1State);
  } else {
    button1State=0;
    
    //Serial.println(button1State);
    //BTSerial.println(button1State);
  }



  delay(2);
  //delay() functions are important!
  //allows the board time to think, the BT port to chill, and prevents glitchiness

  //Receiving from Unity to the ESP32


  if(BTSerial.available()>0){
    //Original reception from 1 value, and we just used 'data'variable
    String unityBTdata = BTSerial.readStringUntil('\n');

    //but now we need to parse multiple values

    //Comma separation code from: https://stackoverflow.com/questions/11068450/arduino-c-language-parsing-string-with-delimiter-input-through-serial-interfa
    int commaIndex = unityBTdata.indexOf(',');
    //  Search for the next comma just after the first
    int secondCommaIndex = unityBTdata.indexOf(',', commaIndex + 1);

    String firstValue = unityBTdata.substring(0, commaIndex);
    String secondValue = unityBTdata.substring(commaIndex + 1, secondCommaIndex);
    //String thirdValue = myString.substring(secondCommaIndex + 1); // To the end of the string


    //Serial.println(data);
    if (firstValue== "0"){
      //seems like expectations for LOW and HIGH are opposite because of wiring
      //here, the digitalWrite HIGH keeps the LED *OFF* while the low is on

      digitalWrite(2,HIGH);
      //BTSerial.println("LED Turned Off!");
      //Serial.println("Serial LED Turned Off!");
    }
    else if (firstValue =="1"){
      digitalWrite(2,LOW);
      //BTSerial.println("LED Turned On!");
      //Serial.println("Serial LED Turned On!");
    }

    //Serial.println(data);
    if (secondValue== "0"){
      digitalWrite(15,HIGH);
      //BTSerial.println("LED Turned Off!");
      //Serial.println("Serial LED Turned Off!");
    }
    else if (secondValue =="1"){
      digitalWrite(15,LOW);
      //BTSerial.println("LED Turned On!");
      //Serial.println("Serial LED Turned On!");
    }
  }

  //yes, another delay() for good measure before looping back again
  delay(2);
  

}
