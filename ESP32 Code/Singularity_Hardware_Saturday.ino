//This is example code using bi-directional communciation with Unity via Bluetooth
//Assuming the Singularity is installed and working in your Unity project and Quest headset
//Functional breakthrough for MIT Reality Hack/Hardware Hack team 13

#include <BluetoothSerial.h>
BluetoothSerial BTSerial;

//The variable we will send
int button1State;

void setup() {
  // put your setup code here, to run once:
  //Serial.begin(9600); //having the normal serial can be useful for debugging when needed
  BTSerial.begin("909FOOTWORK-ESP32"); //Quotes are "Name of your bluetooth device"
  pinMode(2,OUTPUT); //Pin 2 on our provided ES32's uses the onboard blue LED
  pinMode(4,INPUT); //button press on pin 4
}

void loop() {
  
  //Sending values to Unity
  //Button press from uC to destination

  int digitalValue = digitalRead(4);
  // Serial.println(digitalValue);

  BTSerial.println(digitalValue); //this sends constant stream via BT

  if (digitalValue == HIGH) {
    button1State=1;
    //you could send 'digitalValue' straight, or do necessary smoothing/processing/etc here

    //Serial.println(button1State);
    //BTSerial.println(button1State);
  } else {
    button1State=0;
    
    //Serial.println(button1State);
    //BTSerial.println(button1State);
  }



  delay(2);
  //delay() functions are important!
  //allows the board time to think, the BT port to chill, and prevents glitchiness

  //Receiving from Unity to the ESP32

  if(BTSerial.available()>0){
    String data = BTSerial.readStringUntil('\n');
    //Serial.println(data);
    if (data== "0"){
      digitalWrite(2,LOW);
      //BTSerial.println("LED Turned Off!");
      //Serial.println("Serial LED Turned Off!");
    }
    else if (data =="1"){
      digitalWrite(2,HIGH);
      //BTSerial.println("LED Turned On!");
      //Serial.println("Serial LED Turned On!");
    }
  }

  //yes, another delay() for good measure before looping back again
  delay(2);
  

}
