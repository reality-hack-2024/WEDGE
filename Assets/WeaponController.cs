using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public GameObject[] weaponModels; // Array to hold the different weapon models
    public Transform[] shootingPoints; // Array to hold the shooting points for each weapon model
    public GameObject bulletPrefab; // Prefab for the bullet
    public Material[] materials; // Array of materials for the bullet colors
    public float shootingRate = 2.0f; // Rate of shooting (bullets per second)
    public float bulletLifetime = 3.0f; // Time after which bullets are destroyed

    private int currentWeaponIndex = 0; // Index to keep track of the current weapon model
    private float shootingTimer; // Timer to control shooting rate

    void Start()
    {
        SetActiveWeapon(currentWeaponIndex); // Set the initial active weapon
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S)) // Press 'S' to switch weapons
        {
            currentWeaponIndex = (currentWeaponIndex + 1) % weaponModels.Length;
            SetActiveWeapon(currentWeaponIndex);
        }

        // Handle automatic shooting
        shootingTimer += Time.deltaTime;
        if (shootingTimer >= 1f / shootingRate)
        {
            Shoot();
            shootingTimer = 0f;
        }
    }

    void SetActiveWeapon(int index)
    {
        for (int i = 0; i < weaponModels.Length; i++)
        {
            weaponModels[i].SetActive(i == index); // Activate only the current weapon
        }
    }

    void Shoot()
    {
        if (currentWeaponIndex >= shootingPoints.Length)
        {
            Debug.LogError("Shooting point index out of range.");
            return;
        }

        Transform shootingPoint = shootingPoints[currentWeaponIndex];
        if (!shootingPoint)
        {
            Debug.LogError("Shooting point not set for the current weapon model.");
            return;
        }

        GameObject bullet = Instantiate(bulletPrefab, shootingPoint.position, Quaternion.identity);
        bullet.GetComponent<Renderer>().material = materials[currentWeaponIndex % materials.Length];
        Rigidbody rb = bullet.AddComponent<Rigidbody>();
        rb.AddForce(shootingPoint.forward * 500f); // Adjust force as necessary
        Destroy(bullet, bulletLifetime);
    }
}

