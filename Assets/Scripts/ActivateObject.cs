using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateObject : MonoBehaviour
{
    public GameObject obj; // GameObject to activate or deactivate

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X)) // Press X to set object active
        {
            SetActive();
        }
        else if (Input.GetKeyDown(KeyCode.Z)) // Press Z to set object inactive
        {
            SetInactive();
        }
    }

    public void SetActive()
    {
        this.obj.SetActive(true);
    }

    public void SetInactive()
    {
        this.obj.SetActive(false);
    }
}
