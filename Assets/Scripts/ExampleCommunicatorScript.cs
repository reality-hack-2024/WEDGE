//mine

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sngty; //Using Singularity for ESP32

public class ExampleCommunicatorScript : MonoBehaviour
{
    public SingularityManager mySingularityManager;
    List<DeviceSignature> pairedDevices;
    DeviceSignature myDevice = new DeviceSignature();

    public Renderer rend;

    public DeviceSignature GetDevice() { return myDevice; }


    // Start is called before the first frame update
    void Start()
    {
        List<DeviceSignature> pairedDevices = mySingularityManager.GetPairedDevices();

        //If you are looking for a device with a specific name (in this case exampleDeviceName):
        for (int i = 0; i < pairedDevices.Count; i++)
        {
            if ("WORKINGWEDGE-ESP32".Equals(pairedDevices[i].name))
            {
                myDevice = pairedDevices[i];
                break;
            }
        }

        if (!myDevice.Equals(default(DeviceSignature)))
        {
            //Do stuff to connect to the device here

            mySingularityManager.ConnectToDevice(myDevice);
        }
    }

    //KEEP IN MIND IF YOU ARE USING THIS WITH AN ANDROID DEVICE YOU WON'T BE ABLE TO READ LOGS FROM UNITY.
    //You'll have to read the logcat logs through USB debugging. Or just display your messages on a GUI instead of Debug.Log.
    public void onConnected()
    {
        Debug.Log("Connected to device!");

    }

    public void onMessageRecieved(string message)
    {
        //Debug.Log("Message recieved from device: " + message);
        /*if (message == "1")
        {
            rend.material.SetColor("_Color", Color.red);
        } else
        {
            rend.material.SetColor("_Color", Color.white);
        }*/
    }

    public void onError(string errorMessage)
    {
        Debug.LogError("Error with Singularity: " + errorMessage);
    }

    // Update is called once per frame
    void Update()
    {

        //Make sure you're connected to the device before you do this or it won't work


        //mySingularityManager.sendMessage("0", myDevice);
    }
}
