using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sngty; //Using Singularity for ESP32
using TMPro;

public class bt_interaction_testing : MonoBehaviour
{
    public SingularityManager mySingularityManager;

    public ExampleCommunicatorScript myCommunicatorScript;

    public TextMeshProUGUI debugText;




    string lastValueButton1 = "0";
    string lastValueButton2 = "0";





    //public DeviceSignature myDevice = new DeviceSignature();

    public GameObject changeColor;
    private Renderer rend;





    //"Scene" or scenario specific references
    int scenarioTrackingNum = 1;
    public GameObject FirstScenarioObject;
    public GameObject candleBoxObject;
    public GameObject CandleFlameActive;

    public GameObject SecondScenarioObject;
    int gunSelection = 1;
    public GameObject shootingSurface;
    public GameObject gun1;
    public GameObject gun2;
    public GameObject gun3;

    public GameObject ThirdScenarioObject;
    int cubeRowSelection = 1;
    int cubeColumnSelection = 1;
    public GameObject cubeRow1;
    public GameObject cubeRow2;
    public GameObject cubeRow3;

    public GameObject FourthScenarioObject;
    public GameObject questButton1Obj;
    public GameObject questButton2Obj;




    //The entire message you send *to* ESP32 *from* Unity
    string stringToESP32;
    //values that will make up the string
    string value1ToESP = "0";
    string value2ToESP = "0";

    //The entire message you receive *from* the ESP32 *to* Unity
    string stringFromESP32 = "0,0"; //0 to start

    // Start is called before the first frame update
    void Start()
    {
        rend = changeColor.GetComponent<Renderer>();

        CandleFlameActive.active = false;

}

public void rightControllerTriggerBluetoothPress()
    {
        value1ToESP = "1";

        scenarioTrackingNum++;
    }

    public void rightControllerTriggerBluetoothRelease()
    {
        value1ToESP = "0";
    }

    public void rightControllerGripBluetoothPress()
    {
        value2ToESP = "1";
        scenarioTrackingNum--;
    }

    public void rightControllerGripBluetoothRelease()
    {
        value2ToESP = "0";
    }

    public void ChangeObjectColorBlue()
    {
        rend.material.SetColor("_Color", Color.blue);
        //mySingularityManager.sendMessage("1\n", myDevice);
        //mySingularityManager.sendMessage("1\n", myCommunicatorScript.GetDevice());
    }

    public void ChangeObjectColorWhite()
    {
        rend.material.SetColor("_Color", Color.white);
        //mySingularityManager.sendMessage("0\n", myCommunicatorScript.GetDevice());
    }

    public void ChangeObjectColorRed()
    {
        rend.material.SetColor("_Color", Color.red);



        //debugText.text = message;
        //Debug.Log("Sphere recieved from device: " + message);


        //Strin parsing example
        //string s = "name,place,age";
        //string[] pieces = s.Split(','); // separate strings "name" "place" and "age"


        //string[] readings = message.Split(',');
        //for now, we will assume all values are numbers, seperated by commas

        /*if (readings[0]!=null)
        {
            if (readings[0] == "1")
            {
                rend.material.SetColor("_Color", Color.red);
            }
        }*/

        /*if (message == "1")
        {
            rend.material.SetColor("_Color", Color.red);
        }*/

    }

    public void sendStringToESP32()
    {
        mySingularityManager.sendMessage(stringToESP32 + "\n", myCommunicatorScript.GetDevice());
    }

    public void receiveStringFromESP32(string btSerialString)
    {
        stringFromESP32 = btSerialString;

        debugText.text = stringFromESP32;

        //Debug.Log("ESP32 Message Incoming!!!!");
    }

    // Update is called once per frame
    void Update()
    {
        //The entire message you want to send to ESP32 from Unity
        stringToESP32 = value1ToESP +","+ value2ToESP;

        if (Time.frameCount % 5 == 0)
        {
            mySingularityManager.sendMessage(stringToESP32+"\n", myCommunicatorScript.GetDevice());
        }







        //Debug.Log("From ESP32:" + stringFromESP32);
        //Evaluating what we get from the ESP32 and what we want to do with it
        string[] readings = stringFromESP32.Split(',');

        //First value turns sphere magenta
        if (readings[0] != null)
        {
            //if (readings[0] == "1")  //the old way of doing things. Now we need 'rising edge' detection
            int lastInt1 = int.Parse(lastValueButton1);
            int currentInt1 = int.Parse(readings[0]);

            if (currentInt1 + lastInt1 == 1) //If we go from 0 to 1, then the difference will be 1 (and NOT -1)
            {
                //rend.material.SetColor("_Color", Color.magenta);
                Debug.Log("ESP32 Value 1 = 1");
                
                //Visual debug sphere
                ChangeObjectColorBlue();


                //First scenario Candle Box
                if(CandleFlameActive.active == true)
                {
                    CandleFlameActive.active = false;
                }
                else
                {
                    CandleFlameActive.active = true;
                }



                //Second scenario Weapons
                gunSelection++;

                if (gunSelection > 3)
                {
                    gunSelection = 1;
                } else if (gunSelection < 1)
                {
                    gunSelection = 3;
                }

                switch (gunSelection)
                {
                    case 1:
                        gun1.active = true;
                        gun2.active = false;
                        gun3.active = false;
                        break;
                    case 2:
                        gun1.active = false;
                        gun2.active = true;
                        gun3.active = false;
                        break;
                    case 3:
                        gun1.active = false;
                        gun2.active = false;
                        gun3.active = true;
                        break;
                }




                //Third Scenario Cubes
                cubeRowSelection++;

                if (cubeRowSelection > 3)
                {
                    cubeRowSelection = 1;
                }
                else if (cubeRowSelection < 1)
                {
                    cubeRowSelection = 3;
                }

                //reset position before changing
                cubeRow1.transform.localPosition = new Vector3(0.0f, -3.0f, 0.0f);
                cubeRow2.transform.localPosition = new Vector3(0.0f, -1.5f, 0.0f);
                cubeRow3.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

                //change position based on row
                switch (cubeRowSelection)
                {
                    case 1:
                        //cubeRow1.transform.localPosition = new Vector3(0.0f, -3.0f, 2.0f);
                        cubeRow1.transform.localPosition = new Vector3(0.0f, -3.0f, -0.50f);
                        cubeRow2.transform.localPosition = new Vector3(0.0f, -1.5f, 0.0f);
                        cubeRow3.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
                        break;
                    case 2:
                        cubeRow1.transform.localPosition = new Vector3(0.0f, -3.0f, 0.0f);
                        cubeRow2.transform.localPosition = new Vector3(0.0f, -1.5f, -0.50f);
                        cubeRow3.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
                        break;
                    case 3:
                        cubeRow1.transform.localPosition = new Vector3(0.0f, -3.0f, 0.0f);
                        cubeRow2.transform.localPosition = new Vector3(0.0f, -1.5f, 0.0f);
                        cubeRow3.transform.localPosition = new Vector3(0.0f, 0.0f, -0.50f);
                        break;
                }


                //Fourth scenario Quest Controller

                if (questButton1Obj.active == false)
                {
                    questButton1Obj.active = true;
                }
                else
                {
                    questButton1Obj.active = false;
                }

                

            }
        }

        //Second value turns sphere green
        if (readings[1] != null)
        {

            //same dance as above


            //if (readings[1] == "1")//the old way of doing things. Now we need 'rising edge' detection
            int lastInt2 = int.Parse(lastValueButton2);
            int currentInt2 = int.Parse(readings[1]);

            if (currentInt2 + lastInt2 == 1) //If we go from 0 to 1, then the difference will be 1 (and NOT -1)
            {
                //rend.material.SetColor("_Color", Color.green);
                Debug.Log("ESP32 Value 2 = 1");

                //Visual debug sphere
                ChangeObjectColorRed();




                //First scenario Candle Box
                candleBoxObject.transform.Rotate(0f, 1000 * Time.deltaTime, 0f, Space.Self);



                //Second scenario Weapons
                shootingSurface.transform.Rotate(0f, 1000 * Time.deltaTime, 0f, Space.Self);




                //Third scenario Cubes
                //transform.GetChild(0);
                //cubeRowSelection++;

                cubeColumnSelection++;

                if (cubeColumnSelection > 3)
                {
                    cubeColumnSelection = 1;
                }
                else if (cubeColumnSelection < 1)
                {
                    cubeColumnSelection = 3;
                }

                foreach (Transform child in cubeRow1.transform)
                {
                    child.gameObject.GetComponent<Renderer>().material.color = Color.white;
                }
                foreach (Transform child in cubeRow2.transform)
                {
                    child.gameObject.GetComponent<Renderer>().material.color = Color.white;
                }
                foreach (Transform child in cubeRow3.transform)
                {
                    child.gameObject.GetComponent<Renderer>().material.color = Color.white;
                }

                switch (cubeColumnSelection)
                {
                    case 1:                        
                        cubeRow1.transform.GetChild(cubeColumnSelection-1).GetComponent<Renderer>().material.color = Color.yellow;
                        cubeRow2.transform.GetChild(cubeColumnSelection-1).GetComponent<Renderer>().material.color = Color.yellow;
                        cubeRow3.transform.GetChild(cubeColumnSelection-1).GetComponent<Renderer>().material.color = Color.yellow;
                        break;
                    case 2:
                        cubeRow1.transform.GetChild(cubeColumnSelection-1).GetComponent<Renderer>().material.color = Color.yellow;
                        cubeRow2.transform.GetChild(cubeColumnSelection-1).GetComponent<Renderer>().material.color = Color.yellow;
                        cubeRow3.transform.GetChild(cubeColumnSelection-1).GetComponent<Renderer>().material.color = Color.yellow;
                        break;
                    case 3:
                        cubeRow1.transform.GetChild(cubeColumnSelection-1).GetComponent<Renderer>().material.color = Color.yellow;
                        cubeRow2.transform.GetChild(cubeColumnSelection-1).GetComponent<Renderer>().material.color = Color.yellow;
                        cubeRow3.transform.GetChild(cubeColumnSelection-1).GetComponent<Renderer>().material.color = Color.yellow;
                        break;
                }



                //Fourth scenario Quest Controller

                if (questButton2Obj.active == false)
                {
                    questButton2Obj.active = true;
                }
                else
                {
                    questButton2Obj.active = false;
                }

            }
        }

        if (scenarioTrackingNum > 4)
        {
            scenarioTrackingNum = 1;
        } else if (scenarioTrackingNum < 1)
        {
            scenarioTrackingNum = 4;
        }

            //scenarioTrackingNum
            switch (scenarioTrackingNum)
        {
            case 1:
                FirstScenarioObject.active = true;
                SecondScenarioObject.active = false;
                ThirdScenarioObject.active = false;
                FourthScenarioObject.active = false;
                break;
            case 2:
                FirstScenarioObject.active = false;
                SecondScenarioObject.active = true;
                ThirdScenarioObject.active = false;
                FourthScenarioObject.active = false;
                break;
            case 3:
                FirstScenarioObject.active = false;
                SecondScenarioObject.active = false;
                ThirdScenarioObject.active = true;
                FourthScenarioObject.active = false;
                break;
            case 4:
                FirstScenarioObject.active = false;
                SecondScenarioObject.active = false;
                ThirdScenarioObject.active = false;
                FourthScenarioObject.active = true;
                break;
        }







        //comparing last values for 'falling edge' button functionality
        lastValueButton1 = readings[0];
        lastValueButton2 = readings[1];

    }
}
