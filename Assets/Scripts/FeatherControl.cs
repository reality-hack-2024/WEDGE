using UnityEngine;

public class FeatherControl : MonoBehaviour
{
    private Rigidbody rb;
    private AudioSource audioSource;
    public float liftForce = 5f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Alpha0))
        {
            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }

            rb.AddForce(Vector3.up * liftForce, ForceMode.Impulse);
        }
        else if (audioSource.isPlaying)
        {
            audioSource.Stop();
        }
    }
}
