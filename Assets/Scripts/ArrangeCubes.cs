using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrangeCubes : MonoBehaviour
{
    public GameObject[] cubes; // Array to hold your cube references
    public float distance = 2.0f; // Distance between each cube along the Z-axis

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X)) // Detect spacebar press for X-axis alignment
        {
            AlignCubesX();
        }

        if (Input.GetKeyDown(KeyCode.Z)) // Detect Z key press for Z-axis arrangement
        {
            ArrangeCubesZ();
        }
    }

    void AlignCubesX()
    {
        if (cubes.Length == 0) return;

        float alignPositionX = cubes[0].transform.position.x; // Use X position of the first cube as the alignment position

        foreach (GameObject cube in cubes)
        {
            Vector3 newPosition = cube.transform.position;
            newPosition.x = alignPositionX;
            cube.transform.position = newPosition;
        }
    }

    void ArrangeCubesZ()
    {
        if (cubes.Length == 0) return;

        float currentZPosition = cubes[0].transform.position.z; // Start at the Z position of the first cube

        foreach (GameObject cube in cubes)
        {
            Vector3 newPosition = cube.transform.position;
            newPosition.z = currentZPosition; // Set Z position
            cube.transform.position = newPosition;

            currentZPosition -= distance; // Increment Z position for the next cube
        }
    }
}
