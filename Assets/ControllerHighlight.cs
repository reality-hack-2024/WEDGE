using UnityEngine;

public class ControllerHighlight : MonoBehaviour
{
    public GameObject sphere0; // Assign the first highlight sphere
    public GameObject sphere1; // Assign the second highlight sphere
    public GameObject sphereBoth; // Assign the third highlight sphere

    void Update()
    {
        bool key0Pressed = Input.GetKey(KeyCode.Alpha0);
        bool key1Pressed = Input.GetKey(KeyCode.Alpha1);

        // Highlight the third sphere if both keys are pressed
        if (key0Pressed && key1Pressed)
        {
            sphere0.SetActive(false);
            sphere1.SetActive(false);
            sphereBoth.SetActive(true);
        }
        else
        {
            // Highlight only the first sphere if '0' is pressed
            if (key0Pressed)
            {
                sphere0.SetActive(true);
            }
            else
            {
                sphere0.SetActive(false);
            }

            // Highlight only the second sphere if '1' is pressed
            if (key1Pressed)
            {
                sphere1.SetActive(true);
            }
            else
            {
                sphere1.SetActive(false);
            }

            // Make sure the third sphere is not active if both keys are not pressed
            sphereBoth.SetActive(false);
        }
    }
}
