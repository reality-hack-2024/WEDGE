using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;
using Newtonsoft.Json;

public class WeatherTestScript : MonoBehaviour
{
    private readonly string apiKey = "ZrOfXVBFI1xG3raR7ZYICF8oXsuI5Xq0";
    public string latitude = "35.7796";  // demo latitude
    public string longitude = "-78.6382"; // demo longitude

    public TextMeshProUGUI temperatureText;
    public TextMeshProUGUI feelsLikeText;
    public TextMeshProUGUI windSpeedText;
    public TextMeshProUGUI humidityText;
    public TextMeshProUGUI precipitationProbabilityText;

    void Start()
    {
        StartCoroutine(GetWeatherData());
    }

    IEnumerator GetWeatherData()
    {
        string url = $"https://api.tomorrow.io/v4/weather/forecast?location={latitude},{longitude}&apikey={apiKey}";
        UnityWebRequest request = UnityWebRequest.Get(url);
        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError(request.error);
        }
        else
        {
            Debug.Log("Raw JSON: " + request.downloadHandler.text); // Log raw JSON response
            var weatherData = JsonConvert.DeserializeObject<WeatherApiResponse>(request.downloadHandler.text);

            if (weatherData?.Timelines?.Minutely != null && weatherData.Timelines.Minutely.Count > 0)
            {
                Debug.Log("Minutely count: " + weatherData.Timelines.Minutely.Count);
                var firstInterval = weatherData.Timelines.Minutely[0];
                temperatureText.text = "Temperature: " + firstInterval.Values.Temperature + "°C";
                feelsLikeText.text = "'Feels Like': " + firstInterval.Values.TemperatureApparent + "°C";
                windSpeedText.text = "Wind Speed: " + firstInterval.Values.WindSpeed + " m/s";
                humidityText.text = "Humidity: " + firstInterval.Values.Humidity + "%";
                precipitationProbabilityText.text = "Precipitation %: " + firstInterval.Values.PrecipitationProbability + "%";
            }
            else
            {
                Debug.LogError("Minutely data is empty or not in the expected format.");
            }
        }
    }
}