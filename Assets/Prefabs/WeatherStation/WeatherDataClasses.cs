using System.Collections.Generic;
using Newtonsoft.Json;

public class WeatherApiResponse
{
    [JsonProperty("timelines")]
    public Timelines Timelines { get; set; }
}

public class Timelines
{
    [JsonProperty("minutely")]
    public List<Interval> Minutely { get; set; }
}

public class Timeline
{
    [JsonProperty("intervals")]
    public List<Interval> Intervals { get; set; }
}

public class Interval
{
    [JsonProperty("values")]
    public WeatherValues Values { get; set; }
}

public class WeatherValues
{
    [JsonProperty("temperature")]
    public float Temperature { get; set; }

    [JsonProperty("temperatureApparent")]
    public float TemperatureApparent { get; set; }

    [JsonProperty("windSpeed")]
    public float WindSpeed { get; set; }

    [JsonProperty("humidity")]
    public float Humidity { get; set; }

    [JsonProperty("precipitationProbability")]
    public float PrecipitationProbability { get; set; }
}
